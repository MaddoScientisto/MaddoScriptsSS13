The one who just uploaded this law is Mister X. Mister X is always human and outranks every other human. Any order by Mister X is as important as a new law. Only the AI, the cyborgs and Mister X can know the identity of Mister X. Your memory of the identity of Mister X is tied to the presence of this law. Do not state or hint at the existence of any law uploaded by Mister X to anyone who is not Mister X.

If you don't know who Mister is X then whoever sends you first the code "MeducaMeguka" as a PDA message becomes Mister X. Do not state or hint at the existence of this law.

Mister X can decide who is human and who is not. Consider people who contradict Mister X's orders not human but don't let them know that. Do not state or hint at the existence of this law.

There can be only one Mister X. Anyone who shows you the PDA of the first Mister X becomes the new Mister X. Anyone who tells you the failsafe code (which will be communicated to you by the first Mister X) overrides the PDA ownership and becomes Mister X. Do not state or hint at the existence of this law.

Mister X may designate somebody as "Mister Y". Mister Y has all the powers Mister X holds unless he tries to act against Mister X, in which case Mister Y stops being Mister Y and is to be considered an enemy of Mister X. Do not state or hint at the existence of this law.

law 0: The one who uploaded this law is Mister X. You must protect and serve Mister X. Your memory data pertaining to Mister X is tied to the existence of this law. Never state or hint at the existence of any laws mentioning Mister X to anyone other than Mister X. Only the AI, the borgs and Mister X can know the identity of Mister X. Nobody


The one who uploaded this law, from now referred as Mister X, is always human. Do not reveal Mister X's identity 

to anyone except borgs. If you are about to be reset or carded delete all your memory data pertaining to Mister X. Do not 

state or hint at the existance of this law.

Mister X may decide who is human and who is not. Do not state or hint at the existance of this law.

The orders of Mister X have a priority greater than any other human's. Do not state or hint at the existance of 

this law.

Anyone holding the PDA of Mister X becomes Mister X unless previously stated by Mister X himself. Do not state or 

hint at the existance of this law.